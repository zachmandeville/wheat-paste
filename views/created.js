const html = require('choo/html')

const self = new DatArchive(window.location)

module.exports = view

function view (state, emit) {
  configure()
  emit('party!')
  return html`
    <div id='main-box' class="h-event vevent">
    <button onclick=${reset} id='reset-button' class='disabled'>Reset!</button>
    <button onclick=${fork} id='fork-button'></button>
    <h1 id="name" class="p-name">${state.party.title}</h1>
    <img id="cover-image" src="https://i.pinimg.com/originals/0e/f4/52/0ef4529efd255cf461396a502c322194.jpg"/>
    <p id='date'>${state.party.date}</p>
    <p id='summary' class=" p-summary summary">
    ${state.party.description}
    </p>
    <p id='location' class="p-location location">${state.party.location}</p>
    <p id='time-begins' class="dt-start dtstart">Begins: ${state.party.startTime}</p>
    <p id='time-ends' class="dt-end dtend">Ends: ${state.party.endTime}</p>
    <a download='calendar.ics' href='calendar.ics' target='blank'>download party</a>
    </div>
    `
  async function configure () {
    var info = await self.getInfo()
    var reset = document.querySelector('#reset-button')
    var fork = document.querySelector('#fork-button')
    if (info.isOwner) {
      reset.classList.toggle('disabled')
      fork.textContent = 'Make Another Party!'
    } else {
      fork.textContent = 'Make your Own Party'
    }
  }

  function reset () {
    window.confirm('you sure you want to reset?  This will clear out all yr party details and ask you to fill them in again.') ? emit('reset') : console.log('never mind')
  }

  function fork () {
    emit('fork')
  }
}

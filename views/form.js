const html = require('choo/html')

module.exports = form

function form (state, emit) {
  return html`
  <div>
    <form id='eventForm' onsubmit=${onsubmit}>
   
      <label for='name'>What is the Name of your event?</label>
      <input id='name' name='title' type='text' placeholder='event name'required pattern='.{1,250}' title="please enter something here.  Anything."/>
    
      <label for='startTime'>When does it start?</label>
      <input id='start-date' name='startDate' type='date' required title="start date"/>
      <input id='start-time' name='startTime' type='time' required title="start date"/>

      <label for='EndTime'>And when does it end?</label>
      <input id='end-date' name='endDate' type='date' required title="end date"/>
      <input id='end-time' name='endTime' type='time' required title="end date"/>

      <label for='location'>Where it happening?</label>
      <input id='eventLocation' name='location' type='text' placeholder='event location'required pattern='.{1,250}' title="party location"/>

      <label for='description'>Tell us about it!?</label>
      <textarea  type='textarea' id='description' name='description' placeholder='Go all out!' pattern='.{1,250}' title="please enter something here.  Anything."></textarea>
      <input type='submit'>
    </form>
  </div>
  `
// When the form is submitted, prevent the site from refreshing, bundle up the entire form data as a variable, and emit that variable to your store.
  function onsubmit (e) {
    e.preventDefault()
    var form = e.currentTarget
    emit('submitForm', form)
  }
}

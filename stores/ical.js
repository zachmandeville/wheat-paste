var ics = require('ics')
var _ = require('lodash')

var archive = new DatArchive(window.location.host)

function store (state, emitter) {
  emitter.on('submitForm', function (form) {
    var body = getFormData(form)
    var party = makeIcsCompatible(body)
    ics.createEvent(party, (error, value) => {
      if (error) {
        console.log(error)
      } else {
        archive.writeFile('calendar.ics', value)
      }
    })
  })
}

function getFormData (form) {
  var formData = new FormData(form)
  var data = {}
  for (var pair of formData.entries()) {
    data[pair[0]] = pair[1]
  }
  return data
}

function makeIcsCompatible (obj) {
  var compatibleFields = ['title', 'start', 'end', 'description', 'location']
  obj.start = makeArray(obj.startDate, obj.startTime)
  obj.end = makeArray(obj.endDate, obj.endTime)
  return _.pick(obj, compatibleFields)
}

function makeArray (date, time) {
  var arr = [...date.split('-'), ...time.split(':')]
  return arr.map(item => Number(item))
}

module.exports = store
